Connect-K
=========

A #1GAM Game about connecting dots inpired by 4 Connect
http://onegameamonth.com

Made by Vladimir de la Cruz

Contact info
============

Vladimir:
--------
Email:         vladimirdlc@gmail.com<br/>
github:        https://github.com/vladimirdlc<br/>
twitter:       @vladimirdlc<br/>
onegameamonth: http://onegameamonth.com/vladimirdlc<br/>